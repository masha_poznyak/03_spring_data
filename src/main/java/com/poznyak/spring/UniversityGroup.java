package com.poznyak.spring;

import javax.persistence.*;
import java.util.Set;

@Entity
public class UniversityGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    protected UniversityGroup() {
    }

    public Long getId() {

        return id;
    }
    public void setId(Long id) {

        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {

        this.name = name;
    }

    @OneToMany(mappedBy = "universityGroup")
    private Set<Student> student;

    public Set<Student> getStudent() {
        return student;
    }

    public void setStudent(Set<Student> student) {
        this.student = student;
    }

    @ManyToMany(mappedBy = "universityGroup")
    private Set<Discipline> disciplines;

    public Set<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(Set<Discipline> disciplines) {
        this.disciplines = disciplines;
    }
}

