package com.poznyak.spring;

import javax.persistence.*;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;

    protected Student() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @OneToOne
    @JoinColumn(name = "mark_book_id")
    private MarkBook markBook;

    public MarkBook getMarkBook() {
        return markBook;
    }

    public void setMarkBook(MarkBook markBook) {
        this.markBook = markBook;
    }

    @ManyToOne
    @JoinColumn(name = "university_group_id")
    private UniversityGroup universityGroup;

    public UniversityGroup getUniversityGroup() {
        return universityGroup;
    }

    public void setUniversityGroup(UniversityGroup universityGroup) {
        this.universityGroup = universityGroup;
    }

}
