package com.poznyak.spring;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Discipline {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    protected Discipline() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany
    @JoinTable(name="discipline_university_group",
            joinColumns = @JoinColumn(name="discipline_id"),
            inverseJoinColumns = @JoinColumn(name="university_group_id")
    )
    private Set<UniversityGroup> universityGroup;

    public Set<UniversityGroup> getUniversityGroup() {
        return universityGroup;
    }

    public void setUniversityGroup(Set<UniversityGroup> universityGroup) {
        this.universityGroup = universityGroup;
    }
}



